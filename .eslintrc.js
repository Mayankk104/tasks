module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: ['plugin:react/recommended', 'standard-with-typescript', 'prettier'],
    overrides: [
        {
            files: ['*.ts', '*.tsx'],
            rules: {
                '@typescript-eslint/no-floating-promises': ['error', { ignoreIIFE: true }],
            },
        },
    ],
    parserOptions: {
        project: './tsconfig.json',
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    plugins: ['react', 'prettier'],
    rules: {
        'react/react-in-jsx-scope': ['off'],
        'prettier/prettier': [
            'error',
            {
                tabWidth: 4,
                singleQuote: true,
                endOfLine: 'auto',
                arrowParens: 'avoid',
                printWidth: 150,
            },
        ],

        'prefer-arrow-callback': 'error',
        '@typescript-eslint/consistent-type-definitions': 'off',
    },
    settings: {
        react: {
            version: '18.2.0',
        },
    },
};
