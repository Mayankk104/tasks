export type RouteItem = {
    element: JSX.Element;
    path: string;
    protected: boolean;
    Protection?: (args: { element: JSX.Element }) => JSX.Element;
};
