import { Route } from 'react-router-dom';
import PageNotFound from './pages/404';
import Login from './pages/Login';
import Home from './pages/Home';
import ProtectedRoute from './components/routes/Protected';
import Signin from './pages/Signin';
import LoggedIn from './components/routes/Loggedin';
import ReactsPlayground from './pages/reacts';
import DemoPureComponent from './pages/reacts/PureComponent';
import { RouteItem } from './index.d';
import TicTackToe from './pages/TicTacToe';

const routes: RouteItem[] = [
    {
        path: '/',
        element: <Home />,
        protected: true,
        Protection: ProtectedRoute,
    },
    {
        path: '/login',
        element: <Login />,
        protected: true,
        Protection: LoggedIn,
    },
    {
        path: '/signin',
        element: <Signin />,
        protected: true,
        Protection: LoggedIn,
    },
    {
        path: '/react',
        element: <ReactsPlayground />,
        protected: false,
    },
    {
        path: '*',
        element: <PageNotFound />,
        protected: false,
    },
    {
        path: '/react/pure-component',
        element: <DemoPureComponent />,
        protected: false,
    },
    {
        path: '/tic-tac-toe',
        element: <TicTackToe />,
        protected: true,
        Protection: ProtectedRoute,
    },
];

export default routes.map(route => {
    const { path, Protection, element } = route;
    return <Route key={path} path={path} element={route.protected && !(Protection == null) ? <Protection element={element} /> : element} />;
});
