import { Link } from 'react-router-dom';

function ReactsPlayground(): JSX.Element {
    const topics = [
        {
            topic: 'pure-component',
            element: <Link to="/react/pure-component">pure-componenet</Link>,
        },
    ];
    return <>{topics.map(topic => topic.element)}</>;
}

export default ReactsPlayground;
