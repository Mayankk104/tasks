import React, { Component } from 'react';
import styles from './Counter.module.scss';
import { CounterState } from './counter.d';

export default class Counter extends Component<Record<string, unknown>, CounterState>{

    state: Readonly<CounterState> = {
        count: 0
    };

    incrementCountHandler = (): void => {
        this.setState({ count: this.state.count + 1 });
    };

    decrementCountHandler = (): void => {
        this.setState({ count: this.state.count - 1 < 1 ? 0 : this.state.count - 1 });
    };

    render(): JSX.Element {
        console.log('render called for normal component')
        return (
            <div className={styles.section}>
                <h1>Normal Component</h1>
                <h2>{this.state.count}</h2>
                <div>
                    <button className="ts-btn" onClick={this.incrementCountHandler}>
                        +
                    </button>
                    <button className="ts-btn" onClick={this.decrementCountHandler}>
                        -
                    </button>
                </div>
            </div>
        );
    }
}
