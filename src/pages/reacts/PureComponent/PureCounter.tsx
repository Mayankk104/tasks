import { PureComponent } from 'react';
import { CounterState } from './counter.d';
import styles from './PureCounter.module.scss';

export default class PureCounter extends PureComponent<Record<string, unknown>, CounterState> {
    state = {
        count: 0,
    };

    incrementCountHandler = (): void => {
        this.setState({ count: this.state.count + 1 });
    };

    decrementCountHandler = (): void => {
        this.setState({ count: this.state.count - 1 < 1 ? 0 : this.state.count - 1 });
    };

    render(): JSX.Element {
        console.log('render called for pure component')
        return (
            <div className={styles.section}>
                <h1>Pure Component</h1>
                <h2>{this.state.count}</h2>
                <div>
                    <button className="ts-btn" onClick={this.incrementCountHandler}>
                        +
                    </button>
                    <button className="ts-btn" onClick={this.decrementCountHandler}>
                        -
                    </button>
                </div>
            </div>
        );
    }
}
