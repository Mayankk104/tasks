import { Component, ReactNode } from 'react';
import style from './index.module.scss';
import PureCounter from './PureCounter';
import Counter from './Counter';

class DemoPureComponent extends Component<Record<string, unknown>, { count: number }, any> {
    render(): ReactNode {
        return (
            <section className={style.section}>
                <PureCounter />
                <Counter />
            </section>
        );
    }
}

export default DemoPureComponent;
