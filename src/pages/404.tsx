import { Link } from 'react-router-dom';
import { useToast } from '../components/Toast/Toast';
import styles from './404.module.scss';

function PageNotFound(): JSX.Element {
    const toast = useToast();

    const notify = (): void => {
        toast.open({
            message: `Message number ${Math.floor(Math.random() * 1000).toString()}`,
        });
    };

    return (
        <div className={styles.center}>
            <div>
                <h2>404!</h2>
                <p>page not found</p>
                <Link to="/">Home</Link>
                <br />
                <div className={styles['btn-container']}></div>
                <button onClick={notify} className="ts-btn">
                    notify
                </button>
            </div>
        </div>
    );
}

export default PageNotFound;
