import { ChangeEventHandler, FormEventHandler, useState } from 'react';
import styles from './Signin.module.scss';
import { useToast } from '../components/Toast/Toast';
import API from '../services/API';
import { useNavigate } from 'react-router-dom';

function Signin(): JSX.Element {
    const navigate = useNavigate();
    const toast = useToast();
    const api = new API();
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    const inputChangeHandler: ChangeEventHandler<HTMLInputElement> = e => {
        if (e.target.name === 'email') setEmail(e.target.value);
        if (e.target.name === 'password') setPassword(e.target.value);
        if (e.target.name === 'username') setName(e.target.value);
    };

    const formSubmitHandler: FormEventHandler = (e): void => {
        (async () => {
            e.preventDefault();
            try {
                const user = await api.registerUser('/users', { name, password, email });
                localStorage.setItem('token', user.token);
                navigate('/');
            } catch (e) {
                let message = 'Unknow Error';
                if (e instanceof Error) message = e.message;
                toast.open({ message });
            }
        })();
    };

    return (
        <section className={styles['form-container']}>
            <form className={styles.form} onSubmit={formSubmitHandler}>
                <input type="email" className={styles.input} name="email" value={email} onChange={inputChangeHandler} />
                <input type="password" className={styles.input} name="password" value={password} onChange={inputChangeHandler} />
                <input type="text" className={styles.input} name="username" value={name} onChange={inputChangeHandler} />
                <button className="ts-btn">sign in</button>
            </form>
        </section>
    );
}

export default Signin;
