import { ChangeEventHandler, FormEventHandler, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useToast } from '../components/Toast/Toast';
import styles from './Login.module.scss';
import { AuthContext } from '../context/authentication';

function Login(): JSX.Element {
    const toast = useToast();
    const [credentials, setCredentials] = useState({
        password: '',
        email: 'email',
    });
    const navigate = useNavigate();
    const authContext = useContext(AuthContext);

    const changeHandler: ChangeEventHandler<HTMLInputElement> = e => {
        setCredentials({
            ...credentials,
            [e.target.name as 'email' | 'password']: e.target.value,
        });
    };

    const loginHandler: FormEventHandler<HTMLFormElement> = (e): void => {
        e.preventDefault();
        (async () => {
            try {
                const response = await fetch(`${process.env.BACKEND_URL ?? ''}/users/login`, {
                    method: 'POST',
                    body: JSON.stringify({ ...credentials }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

                if (response.status !== 200) throw Error('Invalid Credetials');

                const data = await response.json();
                authContext.login(data.token);
                toast.open({ message: 'Logged In Successfuly ' });
                navigate('/');
            } catch (e) {
                console.log(e);
                toast.open({ message: 'Invalid Credetials' });
            }
        })();
    };
    return (
        <div className={styles['form-container']}>
            <form onSubmit={loginHandler} className={styles.form}>
                <input type="email" name="email" onChange={changeHandler} className={styles.input} />
                <input type="password" name="password" onChange={changeHandler} className={styles.input} />
                <button>sigin</button>
            </form>
        </div>
    );
}

export default Login;
