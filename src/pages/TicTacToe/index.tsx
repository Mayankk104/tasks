import React from 'react';
import Board from '../../components/Board';

export default function TicTackToe(): JSX.Element {
    return (
        <section className="section">
            <Board />
        </section>
    );
}
