import { ChangeEvent, Component, Context, FormEvent } from 'react';
import API from '../services/API';
import styles from './Home.module.scss';
import { ToastContext } from '../components/Toast/Toast';

interface HomeState {
    user: { name: string };
    tasks: Array<{ description: string; _id: string }>;
    description: string;
    task: string;
}

class Home extends Component {
    state: Readonly<HomeState> = {
        user: {
            name: '',
        },
        tasks: [],
        description: '',
        task: '',
    };

    static contextType?: Context<any> | undefined = ToastContext;
    declare context: React.ContextType<typeof ToastContext>;

    api = new API();

    async componentDidMount(): Promise<void> {
        try {
            const token = localStorage.getItem('token') ?? '';

            const user = await this.api.get('/users/profile', token);
            const tasks = await this.api.get('/tasks', token);

            this.setState({ user, tasks });
            this.context.open({ message: 'Welcome', autoClose: true });
        } catch (e) {
            this.context.open({ message: 'Something went wrong' });
            this.setState({ user: { name: 'Default' } });
        }
    }

    taskSubmitHandler = (e: FormEvent<HTMLFormElement>): void => {
        (async () => {
            try {
                e.preventDefault();
                const token = localStorage.getItem('token') ?? '';
                const task = { description: this.state.task };
                const { description, _id } = await this.api.set<{
                    description: string;
                    _id: string;
                }>('/tasks', token, task);
                this.setState({ task: '' });
                this.setState({
                    tasks: [...this.state.tasks, { description, _id }],
                });
            } catch (err) {
                this.context.open({ message: 'Something went wrong' });
            }
        })();
    };

    taskDeleteHandler(id: string): void {
        (async () => {
            try {
                const token = localStorage.getItem('token') ?? '';
                await this.api.delete(`/tasks/${id}`, token);

                this.setState({ tasks: this.state.tasks.filter(task => task._id !== id) });
            } catch (e) {
                this.context.open({ message: 'Something went wrong' });
            }
        })();
    }

    taskInputChangeHandler = (e: ChangeEvent<HTMLInputElement>): void => {
        this.setState({ task: e.target.value });
    };

    override render(): JSX.Element {
        return (
            <section className={styles.section}>
                {/* <h1>Welcome, {this.state.user.name}</h1> */}
                <form action="" onSubmit={this.taskSubmitHandler}>
                    <input value={this.state.task} name="task" placeholder="add task" onChange={this.taskInputChangeHandler} className="input" />
                </form>
                <ul className={styles.list}>
                    {this.state.tasks.map((task: any) => (
                        <li key={task._id}>
                            <section>
                                <input type="checkbox" />
                                <p>{task.description}</p>
                            </section>
                            <button
                                onClick={() => {
                                    this.taskDeleteHandler(task._id);
                                }}
                                className="ts-btn"
                            >
                                delete
                            </button>
                        </li>
                    ))}
                </ul>
            </section>
        );
    }
}

export default Home;
