import { Routes } from 'react-router-dom';
import Navbar from './layouts/Navbar';
import routes from './index.routes';
import { AuthContextProvider } from './context/authentication';

function App(): JSX.Element {
    return (
        <AuthContextProvider>
            <>
                <Navbar />
                <Routes>{routes}</Routes>
            </>
        </AuthContextProvider>
    );
}

export default App;
