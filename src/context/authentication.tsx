import { FC, ReactNode, createContext, useEffect, useState } from 'react';

interface AuthState {
    isAuth: boolean;
    token: null | string;
}
interface AuthContextValue {
    authState: AuthState;
    logout: () => void;
    login: (token: string) => void;
}

export const AuthContext = createContext<AuthContextValue>({ authState: { isAuth: false, token: null }, login: token => {}, logout: () => {} });

export const AuthContextProvider: FC<{ children: ReactNode }> = props => {
    const [authState, setAuthState] = useState<AuthState>({ isAuth: false, token: null });

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token !== null && token?.length > 0) {
            setAuthState({ isAuth: true, token });
        } else {
            setAuthState({ isAuth: false, token: null });
        }
    }, []);

    function logout(): void {
        localStorage.removeItem('token');
        setAuthState({ isAuth: false, token: null });
    }

    function login(token: string): void {
        localStorage.setItem('token', token);
        setAuthState({ isAuth: true, token });
    }

    return <AuthContext.Provider value={{ authState, logout, login }}>{props.children}</AuthContext.Provider>;
};
