import React, { useState } from 'react';
import style from './index.module.scss';
import cross from '../../../public/images/cross.svg';
import circle from '../../../public/images/circle.svg';

export default function Board(): JSX.Element {
    const [boardState] = useState([
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]);
    return (
        <div className={style.board}>
            {boardState.map((boardRow, i) => {
                return boardRow.map((boardCell, j) => {
                    return (
                        <div className={style['board-item']} key={`${i}${j}`}>
                            {(i + j) % 2 === 0 ? <img src={cross} width={'100%'} /> : <img src={circle} width={'100%'} />}
                        </div>
                    );
                });
            })}
        </div>
    );
}
