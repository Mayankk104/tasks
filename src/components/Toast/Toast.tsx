import { createContext, FC, PropsWithChildren, useState, useMemo, useContext, useRef, useEffect } from 'react';
import { createPortal } from 'react-dom';
import styles from './Toast.module.scss';

const ToastContext = createContext<{ open: (toast: ToastRequestBody) => void }>({ open: toast => {} });
const useToast = (): { open: (toast: ToastRequestBody) => void } => useContext(ToastContext);

const Toast: FC<ToastProps> = props => {
    const toastRef = useRef(null);
    useEffect(() => {
        if (props.autoClose === true) {
            setTimeout(() => {
                props.close(props.id, toastRef);
            }, 5000);
        }
    }, []);

    return (
        <li ref={toastRef} className={styles.toast} key={props.id}>
            {props.message}
            <button
                onClick={() => {
                    props.close(props.id, toastRef);
                }}
                className={styles['toast-button']}
            >
                x
            </button>
        </li>
    );
};

const ToastProvider: FC<PropsWithChildren> = props => {
    const [toasts, setToast] = useState<ToastBody[]>([]);

    const open = (content: ToastRequestBody): void => {
        const id = Math.random().toString();
        setToast(currentToasts => [...currentToasts, { ...content, id, autoClose: content.autoClose === undefined ? false : content.autoClose }]);
    };

    const close = (id: number | string, ref?: any): void => {
        if (ref !== undefined || ref !== null) manuplateRef(ref);
        setTimeout(() => {
            setToast(currentToasts => currentToasts.filter(toast => toast.id !== id));
        }, 2000);
    };

    const manuplateRef = (ref: any): void => {
        ref.current.classList.add(styles['fade-out']);
    };

    const contextValue = useMemo(() => ({ open }), []);

    return (
        <ToastContext.Provider value={contextValue}>
            {props.children}
            {createPortal(
                <ul className={styles['toast-container']}>
                    {toasts.map(toast => {
                        return (
                            <Toast
                                manuplateRef={manuplateRef}
                                id={toast.id}
                                key={toast.id}
                                message={toast.message}
                                autoClose={toast.autoClose}
                                close={(id, ref) => {
                                    close(id, ref);
                                }}
                            />
                        );
                    })}
                </ul>,
                document.getElementById('toast') as HTMLElement
            )}
        </ToastContext.Provider>
    );
};

interface ToastBody {
    message: string;
    id: string;
    autoClose: boolean;
}

interface ToastRequestBody {
    message: string;
    autoClose?: boolean;
}

interface ToastProps extends PropsWithChildren {
    id: string;
    message: string;
    close: (id: string, ref: any) => void;
    autoClose?: boolean;
    key: string | number;
    manuplateRef?: (ref: any) => any;
}

export { ToastProvider, useToast, ToastContext };
