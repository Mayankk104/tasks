import { PropsWithChildren, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../../context/authentication';

export default function ProtectedRoute({ element }: ProtectedRouteProps): JSX.Element {
    const authContext = useContext(AuthContext);

    if (!authContext.authState.isAuth) return <Navigate to="/login" />;

    return element;
}

interface ProtectedRouteProps extends PropsWithChildren {
    element: JSX.Element;
}
