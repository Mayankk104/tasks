import { PropsWithChildren, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../../context/authentication';

function LoggedIn(props: ProtectedRouteProps): JSX.Element {
    const authContext = useContext(AuthContext);

    if (authContext.authState.isAuth) return <Navigate to="/" />;

    return props.element;
}

interface ProtectedRouteProps extends PropsWithChildren {
    element: JSX.Element;
}

export default LoggedIn;
