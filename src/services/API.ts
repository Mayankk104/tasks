class API {
    async get<T>(url: string, token: string): Promise<T> {
        const response = await fetch(`${process.env.BACKEND_URL ?? ''}${url}?limit=10`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        if (response.status !== 200) throw Error("can't find tasks");
        const result = (await response.json()) as Promise<T>;

        return await result;
    }

    async set<T>(url: string, token: string, body: any): Promise<T> {
        const response = await fetch(`${process.env.BACKEND_URL ?? ''}${url}`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        const result = await response.json();
        return result;
    }

    async delete(url: string, token: string): Promise<void> {
        const response = await fetch(`${process.env.BACKEND_URL ?? ''}${url}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
        });

        if (response.status !== 200) throw Error("Can't Delete Task");
        const result = await response.json();
        return result;
    }

    async registerUser(url: string, body: { name: string; password: string; email: string }): Promise<any> {
        const resposnse = await fetch(`${process.env.BACKEND_URL ?? ''}${url}`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
            },
        });

        if (!resposnse.ok) {
            switch (resposnse.status) {
                case 400:
                    throw Error('Bad Request');
                case 500:
                    throw Error('Internal Server Error');
            }
        }

        const result = await resposnse.json();
        return result;
    }
}

export default API;
