import { useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { AuthContext } from '../context/authentication';
import './Navbar.scss';

function Navbar(): JSX.Element {
    const navigate = useNavigate();
    const authConsumer = useContext(AuthContext);

    const logoutHandler = (): void => {
        authConsumer.logout();
        navigate('/login');
    };

    return (
        <nav className="navbar">
            <ul>
                <li>
                    <NavLink to="/react">react</NavLink>
                </li>
                {!authConsumer.authState.isAuth && (
                    <>
                        <li>
                            <NavLink to="login">login</NavLink>
                        </li>
                        <li>
                            <NavLink to="signin">signin</NavLink>
                        </li>
                    </>
                )}
                {authConsumer.authState.isAuth && (
                    <>
                        <li>
                            <NavLink to="/">home</NavLink>
                        </li>
                        <li>
                            <NavLink to="/tic-tac-toe">tic tac toe</NavLink>
                        </li>
                        <li>
                            <button onClick={logoutHandler} className="ts-btn">
                                logout
                            </button>
                        </li>
                    </>
                )}
            </ul>
        </nav>
    );
}

export default Navbar;
