import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { ToastProvider } from './components/Toast/Toast';
import './scss/index.scss';

const rootElement = document.getElementById('root') as HTMLElement;
const root = createRoot(rootElement);

root.render(
    <ToastProvider>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </ToastProvider>
);
