const { merge } = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const common = require('./webpack.config');
const {join} = require('path');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const CleanTerminalPlugin = require('./clear');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'source-map',
    devServer: {
        port: 3000,
        static: join(__dirname, '..' ,'..', 'public'),
        hot: true,
        open: true,
        historyApiFallback: true,
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_module/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: ['react-refresh/babel']
                    },
                },
            },
        ],
    },
    plugins: [
        new CleanTerminalPlugin(),
        new ReactRefreshWebpackPlugin(),
        new Dotenv({
            path: join(__dirname, '..', 'env', '.env'), // load this now instead of the ones in '.env'
            defaults: false, // load '.env.defaults' as the default values if empty.
          }),
    ]
});
