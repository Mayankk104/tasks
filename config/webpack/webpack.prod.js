const { join } = require('path');
const Dotenv = require('dotenv-webpack');
const { merge } = require('webpack-merge');
const common = require('./webpack.config');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const WebpackBundleAnalizer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = merge(common, {
    mode: 'production',
    output: {
        path: join(__dirname, '..', '..', 'dist'),
        filename: 'js/bundle.[fullhash].js',
        clean: true
    },
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_module/,
                loader: 'babel-loader',
            },
        ],
    },
    plugins: [
        new Dotenv({
            path: join(__dirname, '..', 'env', '.env.prod'), 
            defaults: false,
          }), 
        new MiniCssExtractPlugin({ filename: 'styles/[name].[contenthash].css' }),
        new CopyWebpackPlugin({
            patterns: [{ from: 'public/_redirects' }],
        }),
        new CompressionPlugin(),
        new WebpackBundleAnalizer()
    ],
});
